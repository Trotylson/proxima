import pygame
import random
from pygame.math import Vector2

class Mob(object):
    def __init__(self, game, position: tuple=(500,500), speed: float=0.5, velocity: tuple=(0,0), acceleration: tuple=(0,0)):
        # Const
        self.game = game
        self.position = Vector2(position)
        self.velocity = Vector2(velocity)
        self.acc = Vector2(acceleration)
        self.speed = speed
        self.movement = 0
        self.movement_vector = None
        self.is_moving = False
        
        # Configurable
        self.body_radius = 10
        self.attack_radius = 15
        self.field_of_view = 50

    def add_force(self, force):
        self.acc += force
    
    def move(self):
        rnd = random.randint(0, 11)
        if rnd in [1]:
            self.is_moving = True
        if self.is_moving:
            if self.movement <= 0:
                self.is_moving = False
                self.movement = random.randint(20, 50)
                self.movement_vector = random.choice([Vector2(self.speed,0), Vector2(0,self.speed), Vector2(-self.speed,0), Vector2(0,-self.speed)])
            self.add_force(self.movement_vector)
            self.movement -= 1

    def tick(self):
        # Actions
        self.move()
            
        # Physics
        self.velocity *= 0.7
        self.velocity += self.acc
        self.position += self.velocity
        self.acc *= 0

    def draw(self):
        # pygame.draw.rect(self.game.screen, (0,100,255), pygame.Rect(self.position.x,self.position.y,50,50))
        pygame.draw.circle(self.game.screen, (80,10,0), (self.position.x, self.position.y), self.body_radius)