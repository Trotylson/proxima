import pygame
from pygame.math import Vector2

class Player(object):
    def __init__(self, game):
        # Const
        self.game = game
        self.position = Vector2(100,100)
        self.velocity = Vector2(0,0)
        self.acc = Vector2(0,0)
        
        # Configurable
        self.location = 'Drone Hub'
        self.run = 0.6
        self.radius = 10


    def add_force(self, force):
        self.acc += force

    def tick(self):
        # Input
        pressed = pygame.key.get_pressed()
        if pressed[pygame.K_d]:
            self.add_force(Vector2(1,0))
        if pressed[pygame.K_a]:
            self.add_force(Vector2(-1,0))
        if pressed[pygame.K_w]:
            self.add_force(Vector2(0,-1))
        if pressed[pygame.K_s]:
            self.add_force(Vector2(0,1))
        if pressed[pygame.K_LSHIFT]:
            self.run = 0.85
        else: self.run = 0.6

        # Physics
        self.velocity *= self.run

        self.velocity += self.acc
        self.position += self.velocity
        self.acc *= 0

    def draw(self):
        # pygame.draw.rect(self.game.screen, (0,100,255), pygame.Rect(self.position.x,self.position.y,50,50))
        pygame.draw.circle(self.game.screen, (0,80,0), (self.position.x, self.position.y), self.radius)