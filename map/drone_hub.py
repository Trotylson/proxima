from mobs.mob import Mob
from mobs.dog import Dog
from game_objects.object import Object as GameObject
from pygame import Vector2

class DroneHub(object):
    def __init__(self, game):
        self.game = game
        self.wall = GameObject(game)
        self.mob = Mob(game, (600,200), 1)
        self.mob2 = Mob(game)
        self.dog = Dog(game, (500,300), 2)

    def tick(self):
        self.mob.tick()
        self.mob2.tick()
        self.dog.tick()

    def draw_map(self):
        game_window = self.game.resolution
        self.wall.draw_line(color=[255,255,255], position_x=Vector2(5,5), position_y=Vector2(5, game_window.y - 5))
        self.wall.draw_line(color=[255,255,255], position_x=Vector2(5,5), position_y=Vector2(game_window.x - 5, 5))
        self.wall.draw_line(color=[255,255,255], position_x=Vector2(5,game_window.y - 5), position_y=Vector2(game_window.x - 5, game_window.y - 5))
        self.wall.draw_line(color=[255,255,255], position_x=Vector2(game_window.x - 5, 5), position_y=Vector2(game_window.x - 5, game_window.y - 5))
        self.wall.draw_rectangle(color=[40,0,0], position=Vector2(300, 300), width=100, height=50)
        self.mob.draw()
        self.mob2.draw()
        self.dog.draw()