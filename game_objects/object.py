import pygame
from pygame import Vector2

class Object(object):
    def __init__(self, game):
        self.game = game
        # self.position = Vector2()

    def tick(self):
        pass

    def draw_rectangle(self, color: list=[20, 20, 100], position: tuple=Vector2(10, 10), width: int=10, height: int=10):
        # 
        # self.position = Vector2(position.x - width/2, position.y - height/2)
        pygame.draw.rect(self.game.screen, color, pygame.Rect(position.x - width/2, position.y - height/2, width, height))

    def draw_circle(self, color: list=[20,20,100], position: tuple=Vector2(10, 10), radius: int=10):
        pygame.draw.circle(self.game.screen, [color[0], color[1],color[2]], (position.x, position.y), radius)

    def draw_line(self, color: list=[20,20,100], position_x: tuple=Vector2(10, 10), position_y: tuple=Vector2(20,20)):
        # pygame.draw.line(self.game.screen.x ,
        pygame.draw.line(self.game.screen, color, position_x, position_y)