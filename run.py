import pygame
import sys
from pygame import Vector2
from player.player import Player
from mobs.mob import Mob
from map.drone_hub import DroneHub

class Game(object):
    def __init__(self):
        # Configurations
        self.tps_max = 20.0
        # self.resolution = Vector2(1280, 720)
        self.resolution = Vector2(1915, 1020)

        # Initialization
        pygame.init()
        self.screen = pygame.display.set_mode(self.resolution)
        self.tps_clock = pygame.time.Clock()
        self.tps_delta = 0.0

        self.player = Player(self)
        self.map_dronehub = DroneHub(self)

        while True:
            # Events handler
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    sys.exit(0)
                elif event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                    sys.exit(0)

            # Ticking
            self.tps_delta += self.tps_clock.tick() / 1000.0
            while self.tps_delta > 1 / self.tps_max:
                self.tick()

                self.tps_delta -= 1 / self.tps_max

            # Drawing
            self.screen.fill((0,0,0))
            self.draw()
            # pygame.display.update()
            pygame.display.flip()


    def tick(self):
        # Checking inputs / moves
        self.player.tick()
        if self.player.location == 'Drone Hub':
            self.map_dronehub.tick()

    def draw(self):
        # Draw all
        if self.player.location == 'Drone Hub':
            self.map_dronehub.draw_map()
        self.player.draw()

if __name__ == "__main__":
    Game()
